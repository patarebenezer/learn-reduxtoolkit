import { createSlice, nanoid } from "@reduxjs/toolkit";
import { sub } from "date-fns";

const initialState = [
 {
  id: 1,
  title: "Learning Redux toolkit",
  content: "lorem ipmsum dolor sid",
  date: sub(new Date(), { minutes: 10 }).toISOString(),
  reactions: {
   like: 0,
   heart: 0,
   coffee: 0,
  },
 },
 {
  id: 2,
  title: "Learning ReactJS",
  content: "lorem ipmsum dolor sid reactjs bro",
  date: sub(new Date(), { minutes: 7 }).toISOString(),
  reactions: {
   like: 0,
   heart: 0,
   coffee: 0,
  },
 },
 {
  id: 3,
  title: "Learning Redux and Reactjs",
  content: "lorem ipmsum dolor sid reactjs bro",
  date: sub(new Date(), { minutes: 5 }).toISOString(),
  reactions: {
   like: 0,
   heart: 0,
   coffee: 0,
  },
 },
];

const postsSlice = createSlice({
 name: "posts",
 initialState,
 reducers: {
  addPosts: {
   reducer(state, action) {
    state.push(action.payload);
   },
   prepare(title, content, userId) {
    return {
     payload: {
      id: nanoid(),
      title,
      content,
      date: new Date().toISOString(),
      userId,
      reactions: {
       like: 0,
       heart: 0,
       coffee: 0,
      },
     },
    };
   },
  },
  reactionAdded(state, action){
   const { postId, reaction } = action.payload;
   const existingPost = state.find(post => post.id === postId);
   if(existingPost){
    existingPost.reactions[reaction]++;
   }
  }
 },
});

export const getAllPosts = (state) => state.posts;

export const { addPosts, reactionAdded } = postsSlice.actions;

export default postsSlice.reducer;
