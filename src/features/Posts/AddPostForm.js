/* eslint-disable react-hooks/rules-of-hooks */
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAllUsers } from "../users/users.slice";
import { addPosts } from "./posts.slice";
import "./index.css";

const AddPostForm = () => {
 const dispatch = useDispatch();
 const users = useSelector(getAllUsers);
 const [form, setForm] = useState({
  title: "",
  content: "",
  author: "",
 });

 const handleInputChange = (field) => {
  return (e) => {
   setForm((prev) => ({
    ...prev,
    [field]: e.target.value,
   }));
  };
 };

 const onSavePostSubmit = () => {
  if (form.title && form.content && form.author) {
   dispatch(addPosts(form.title, form.content, form.author));

   setForm({
    title: "",
    content: "",
    author: "",
   });
  }
 };

 const canSave = Boolean(form.title) && Boolean(form.content) && Boolean(form.author);
 
 const usersOption = users.map(user => (
  <option key={user.id} value={user.id}>{user.name}</option>
 ));

 return (
  <>
   <div className="only-grid">
    <input
     className="input"
     placeholder="Title"
     type="text"
     value={form.title}
     onChange={handleInputChange("title")}
    />

    <select
     className="input"
     name="author"
     value={form.author}
     placeholder="Author"
     onChange={handleInputChange("author")}
    >
     <option value="">Select Author</option>
     {usersOption}
    </select>

    <textarea
     className="input"
     value={form.content}
     onChange={handleInputChange("content")}
     placeholder="Content"
    />

    <button disabled={!canSave} className="input" type="button" onClick={onSavePostSubmit}>
     Add Post
    </button>
   </div>
  </>
 );
};

export default AddPostForm;
