import { useDispatch } from "react-redux";
import { reactionAdded } from "./posts.slice";

const reactionEmoji = {
 like: "👍",
 heart: "❤️",
 coffee: "☕",
};

const ReactionButton = ({ post }) => {
 const dispatch = useDispatch();
 const reactionBtn = Object.entries(reactionEmoji).map(([name, emoji]) => {
  return (
   <button
    key={name}
    type="button"
    onClick={() => dispatch(reactionAdded({ postId: post.id, reaction: name }))}
   >
    {emoji} {post.reactions[name]}
   </button>
  );
 });
 return <div>{reactionBtn}</div>;
};

export default ReactionButton;
