import { useSelector } from "react-redux";
import { getAllPosts } from "./posts.slice";
import AddPostForm from "./AddPostForm";
import PostAuthor from "./PostAuthor";
import "./index.css";
import TimeAgo from "./TimeAgo";
import ReactionButton from "./ReactionButton";

const Posts = () => {
 const posts = useSelector(getAllPosts);
 const orderedPostsByDate = posts.slice().sort((a, b) => b.date.localeCompare(a.date))
 const renderedPosts = orderedPostsByDate.map((post) => (
  <article className="card" key={post.id}>
   <h3>{post.title}</h3>
   <p>{post.content.substring(0, 100)}</p>
   <PostAuthor userId={post.userId} />
   <TimeAgo timestamp={post.date}/>
   <ReactionButton post={post} />
  </article>
 ));

 return (
  <div style={{ textAlign: "center" }}>
   <h2>POSTS</h2>
   <AddPostForm />
   <div className="grid">{renderedPosts}</div>
  </div>
 );
};

export default Posts;
