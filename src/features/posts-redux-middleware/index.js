import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
 getAllPosts,
 getPostsError,
 fetchPosts,
 getPostsStatus,
} from "./posts.slice";
import AddPostForm from "./AddPostForm";
import PostContainer from "./PostContainer";
import "./index.css";

const PostsMiddleWare = () => {
 const dispatch = useDispatch();
 const posts = useSelector(getAllPosts);
 const status = useSelector(getPostsStatus);
 const error = useSelector(getPostsError);

  let content;
  if(status === 'loading'){
   content = <p>Loading...</p>
  }else if(status === 'succeeded'){
   const orderedPostsByDate = posts.slice().sort((a, b) => b.date.localeCompare(a.date));
   content = orderedPostsByDate.map(post => <PostContainer key={post.id} post={post}/>);
  }else if(status === 'failed'){
   content = <p>{error}</p>
  }

  useEffect(() => {
   status === 'idle' && dispatch(fetchPosts());
  }, [status, dispatch]);
  
 return (
  <div style={{ textAlign: "center" }}>
   <h2>POSTS</h2>
   <AddPostForm />
   <div className="grid">{content}</div>
   {status}
  </div>
 );
};

export default PostsMiddleWare;
