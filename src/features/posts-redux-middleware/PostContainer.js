import React from "react";
import PostAuthor from "./PostAuthor";
import TimeAgo from "./TimeAgo";
import ReactionButton from "./ReactionButton";

const PostContainer = ({ post }) => {
 return (
  <article className="card">
   <h3>{post.title}</h3>
   <p>{post.body.substring(0, 100)}</p>
   <PostAuthor userId={post.userId} />
   <TimeAgo timestamp={post.date} />
   <ReactionButton post={post} />
  </article>
 );
};

export default PostContainer;
