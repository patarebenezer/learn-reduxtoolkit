import { createSlice, nanoid, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { sub } from "date-fns";

const BASE_URL = "https://jsonplaceholder.typicode.com/posts";

const initialState = {
 posts: [],
 status: "idle", // 'idle' || 'loading' || 'succeeded' || 'failed'
 error: null,
};

export const fetchPosts = createAsyncThunk("posts/fetchPosts", async () => {
 try {
  const response = await axios.get(BASE_URL);
  return response.data;
 } catch (error) {
  return error.message;
 }
});

export const addNewPosts = createAsyncThunk("posts/addNewPosts", async (payload) => {
  try {
   const response = await axios.post(BASE_URL, payload);
   return response.data;
  } catch (error) {
   return error.message;
  }
 });

const postsSlice = createSlice({
 name: "posts",
 initialState,
 reducers: {
  addPosts: {
   reducer(state, action) {
    state.posts.push(action.payload);
   },
   prepare(title, content, userId) {
    return {
     payload: {
      id: nanoid(),
      title,
      content,
      date: new Date().toISOString(),
      userId,
      reactions: {
       like: 0,
       heart: 0,
       coffee: 0,
      },
     },
    };
   },
  },
  reactionAdded(state, action) {
   const { postId, reaction } = action.payload;
   const existingPost = state.posts.find((post) => post.id === postId);
   if (existingPost) {
    existingPost.reactions[reaction]++;
   }
  },
 },

 extraReducers(builder) {
  builder
   .addCase(fetchPosts.pending, (state, action) => {
    state.status = "loading";
   })
   .addCase(fetchPosts.fulfilled, (state, action) => {
    state.status = "succeeded";
    let min = 2;
    const loadedPosts = action.payload.map((post) => {
     post.date = sub(new Date(), { minutes: min++ }).toISOString();
     post.reactions = {
      like: 0,
      heart: 0,
      coffee: 0,
     };
     return post;
    });
    state.posts = state.posts.concat(loadedPosts);
   })
   .addCase(fetchPosts.rejected, (state, action) => {
    state.status = 'failed';
    state.error = action.error.message;
   })
   .addCase(addNewPosts.fulfilled, (state, action) => { // line baru addcase untuk add new post
    action.payload.userId = Number(action.payload.userId)
    action.payload.date = new Date().toISOString();
    action.payload.reactions = {
      like: 0,
      heart: 0,
      coffee: 0,
    }
    state.posts.push(action.payload);
   }) // line baru addcase untuk add new post
 },
});

export const getAllPosts = (state) => state.posts.posts;
export const getPostsStatus = (state) => state.posts.status;
export const getPostsError = (state) => state.posts.error;

export const { addPosts, reactionAdded } = postsSlice.actions;

export default postsSlice.reducer;
