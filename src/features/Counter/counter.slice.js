import { createSlice } from "@reduxjs/toolkit";

const initialState = {
 count: 0,
};

export const counterSlice = createSlice({
 // create state
 name: "counter", //nama statenya
 initialState,
 reducers: {
  // actionya mau ngapain
  increment: (state) => {
   state.count += 1;
  },
  decrement: (state) => {
   state.count < 1 ? (state.count = 0) : (state.count -= 1);
  },
  reset: (state) => {
   state.count = 0;
  },
  incrementByAmount: (state, action) => {
   state.count += action.payload
  }
 },
});

export const { increment, decrement, reset, incrementByAmount } = counterSlice.actions; // inisialisasi action

export default counterSlice.reducer; // dibuat secara global biar bisa dipanggil.
