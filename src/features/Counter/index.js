import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { decrement, increment, reset, incrementByAmount } from './counter.slice'

const Counter = () => {
  const dispatch = useDispatch();
  const [incrementAmount, setIncrementAmount] = useState(0)
  const addValue = Number(incrementAmount) || 0
  const resetAll = () => {
    setIncrementAmount(0)
    dispatch(reset())
  }
  const count = useSelector((state) => state.counter.count);
  return (
    <div style={{textAlign: 'center'}}>
      <h1>Counter</h1>
      <p>{count}</p>
      <button style={{marginRight: '10px'}} onClick={() => dispatch(decrement())}>-</button>
      <button onClick={() => dispatch(increment())}>+</button>
      <div>
        <input type="text" value={incrementAmount} onChange={(e) => setIncrementAmount(e.target.value)}/>
        <button onClick={() => dispatch(incrementByAmount(addValue))}>Add Amount</button>
        <button onClick={resetAll}>Reset</button>
      </div>
    </div>
  )
}

export default Counter