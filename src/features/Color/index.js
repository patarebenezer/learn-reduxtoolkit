import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addColors, getAllColors } from "./color.slice";
import "./index.css";

const Color = () => {
 const dispatch = useDispatch();
 const colors = useSelector(getAllColors);
 const renderColor = colors.map((item) => (
  <article className="card" key={item.id}>
   <h3 style={{ color: item.color}}>{item.title}</h3>
   <p style={{ backgroundColor: item.color}} className="color"></p>
  </article>
 ));
 
 const [form, setForm] = useState({
  title: '',
  color: ''
 })

 const handleInputChange = (field) => {
  return (e) => {
   setForm((prev) => ({
    ...prev,
    [field]: e.target.value
   }))
  }
 }

 const onSaveSubmit = () => {
  if(form.title && form.color){
   dispatch(addColors(form.title, '#'+form.color))
  }
  setForm({
   title:'',
   color:''
  })
 }
 return (
  <>
   <h1 className="text-center">Colors</h1>
   <div className="grid">
    <input className="p-4" type="text" placeholder="Title" value={form.title} onChange={handleInputChange("title")}/>
    <input className="p-4" type="text" placeholder="Color Code" value={form.color} onChange={handleInputChange("color")}/>
    <button className="p-4" onClick={onSaveSubmit}>Add Color</button>
   </div>
   <div className="center">
    <div className="container">{renderColor}</div>
   </div>
  </>
 );
};

export default Color;
