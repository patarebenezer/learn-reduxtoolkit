import { createSlice, nanoid } from "@reduxjs/toolkit";

const initialState = [
 {id:1, title: 'Blue', color: '#62CDFF'},
 {id:2, title: 'Red', color: '#E21818'},
 {id:3, title: 'Orange', color: '#FFB84C'}
]

export const colorSlice = createSlice({
 name: 'colors',
 initialState,
 reducers:{
  addColors: {
   reducer(state, action){
    state.push(action.payload)
   },
   prepare(title, color){ // ini berguna untuk menggantikan penulisan payload di viewnya
    return{
     payload:{
      id: nanoid(),
      title,
      color
     }
    }
   }
  }
 }
})

export const { addColors } = colorSlice.actions
export const getAllColors = (state) => state.colors
export default colorSlice.reducer