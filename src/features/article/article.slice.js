import { createAsyncThunk, createSlice, nanoid } from "@reduxjs/toolkit";
import axios from "axios";

const BASE_URL = "https://jsonplaceholder.typicode.com/photos";

const initialState = {
 data: [],
 status: "idle",
};

export const fetchArticle = createAsyncThunk(
 "article/fetchArticle",
 async () => {
  try {
   const res = await axios.get(BASE_URL);
   return res.data.slice(0, 3);
  } catch (error) {
   return error.message;
  }
 }
);

export const articleSlice = createSlice({
 name: "article",
 initialState,
 reducers: {
  // untuk create action entah itu fungsi add atau remove atau apapun yg butuh tindakan
  addArticle: {
   reducer(state, action) {
    state.data.push(action.payload);
   },
   prepare(title, thumbnailUrl) {
    return {
     payload: {
      albumId: 1,
      id: nanoid(),
      title,
      url: "",
      thumbnailUrl,
     },
    };
   },
  },
 },
 /* Cara Lama */
 // extraReducers(builder){
 //  builder
 //  .addCase(fetchArticle.pending, (state, action) => {
 //   state.data = [];
 //   state.status = 'pending'
 //  })
 //  .addCase(fetchArticle.fulfilled, (state, action) => {
 //   state.status = 'success'
 //   state.data = action.payload;
 //  })
 //  .addCase(fetchArticle.rejected, (state, action) => {
 //   state.data = [];
 //   state.status = 'failed'
 //  })
 // }

 /* Cara Baru */
 extraReducers: {
  [fetchArticle.pending]: (state) => {
   state.status = "pending";
  },
  [fetchArticle.rejected]: (state) => {
   state.status = "rejected";
  },
  [fetchArticle.fulfilled]: (state, { payload }) => {
   state.status = "success";
   state.data = payload;
  },
 },
});

export const getAllArticle = (state) => state.article.data;

export const { addArticle } = articleSlice.actions;

export default articleSlice.reducer;
