import { useRef, useState } from "react";
import { useDispatch } from "react-redux";
import { addArticle } from "./article.slice";

const AddArticleForm = () => {
 const dispatch = useDispatch();
 const [form, setForm] = useState({
  filename: null,
  image: null,
  title: "",
 });
 const fileInputRef = useRef(null);

 const uploadImage = (e) => {
  e.preventDefault();
  const file = e.target.files[0];
  const allowedTypes = ["image/jpeg", "image/png", "image/gif"];
  if (allowedTypes.includes(file.type)) {
   setForm({
    filename: file,
    image: URL.createObjectURL(file),
   });
  } else {
   alert("Only JPEG, PNG, and GIF images are allowed.");
   fileInputRef.current.value = ''
  }
 };

 const submitArticle = () => {
  fileInputRef.current.value = "";
  setForm({
   title: "",
  });
  dispatch(addArticle(form.title, form.image));
 };

 return (
  <div className="text-center w-full">
   <img alt="" src={form.image} className="w-5" />
   <input
    type="file"
    accept="image/jpeg, image/png, image/gif"
    name="image"
    onChange={uploadImage}
    ref={fileInputRef}
   />
   <input
    type="text"
    placeholder="Description photo"
    value={form.title}
    onChange={(e) => setForm({ ...form, title: e.target.value })}
   />
   <button onClick={submitArticle}>Add Article</button>
  </div>
 );
};

export default AddArticleForm;
