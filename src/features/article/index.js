import { useSelector } from "react-redux";
import AddArticleForm from "./AddArticleForm";
import { getAllArticle } from "./article.slice";
import "./index.css";

const Article = () => {
 const data = useSelector(getAllArticle);
 const status = useSelector((state) => state.article.status);
 return (
  <>
   <h1 className="text-center">Article</h1>
   <AddArticleForm />
   <div className="center">
    {status === "pending" ? (
     <p>Loading..</p>
    ) : (
     data.map((article) => (
      <div key={article?.id} className="card">
       <img src={article?.thumbnailUrl} alt="" />
       <p>{article?.title}</p>
      </div>
     ))
    )}
   </div>
  </>
 );
};

export default Article;
