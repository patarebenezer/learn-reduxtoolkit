import { configureStore } from "@reduxjs/toolkit";
import counterReducer from "../features/counter/counter.slice";
// import postsReducer from "../features/posts/posts.slice";
import postsMiddlewareReducer from "../features/posts-redux-middleware/posts.slice";
import colorReducer from "../features/color/color.slice";
import usersReducer from "../features/users/users.slice";
import articleReducer from "../features/article/article.slice";

export const store = configureStore({
 reducer:{
  counter: counterReducer,
  // posts: postsReducer,
  posts: postsMiddlewareReducer,
  colors: colorReducer,
  users: usersReducer,
  article: articleReducer
 }
})