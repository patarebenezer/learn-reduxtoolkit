// import Counter from "./features/counter"
// import Color from "./features/color"
// import Posts from "./features/posts"
// import AddPostForm from "./features/Posts/AddPostForm"

import { store } from "./app/store"
import Article from "./features/article"
import { fetchArticle } from "./features/article/article.slice"
// import PostsMiddleWare from "./features/posts-redux-middleware"
import { fetchUsers } from "./features/users/users.slice"

store.dispatch(fetchUsers()) // kalo ga mau deklarasi variable dispatch, pake cara ini bisa.
store.dispatch(fetchArticle())

const App = () => {
  return (
    <>
    {/* <AddPostForm /> */}
    {/* <Posts /> */}
    {/* <Color /> */}
    <Article />
    </>
  )
}

export default App